<?php

namespace Tests\FrontBundle\Controller;

use Tests\ExtendedWebTestCase;

class HomepageControllerTest extends ExtendedWebTestCase
{
    /**
     * Test the front homepage with an ANONYMOUS user
     */
    public function testIndexAsAnonymousUser()
    {
        $this->client = $this->initiateAnonymousClient();
        $this->client->followRedirects();
        $crawler = $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("homepage")')->count());
    }

    /**
     * Test the app homepage with an ROLE_USER user
     */
    public function testIndexAsUser()
    {
        $this->client = $this->initiateUserClient();
        $this->client->followRedirects();
        $crawler = $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("home")')->count());
    }

    /**
     * Test the front homepage with an ROLE_ADMIN user
     */
    public function testIndexAsAdminUser()
    {
        $this->client = $this->initiateAdminClient();
        $this->client->followRedirects();
        $crawler = $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("home")')->count());
    }

}