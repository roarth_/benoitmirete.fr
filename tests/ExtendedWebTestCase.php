<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 25/08/17
 * Time: 15:47
 */
namespace Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExtendedWebTestCase extends WebTestCase
{
    /**
     * @var
     */
    public $client;

    /**
     * Instantiate an anonymous client
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    public function initiateAnonymousClient()
    {
        $this->client = static::createClient();
        return $this->client;
    }

    /**
     * Initiate an User client
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    public function initiateUserClient()
    {
        $this->client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'test-user@me.io',
            'PHP_AUTH_PW' => 'foobar',
        ));
        return $this->client;
    }

    /**
     * Initiate an Admin client
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    public function initiateAdminClient()
    {
        $this->client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'test-admin@me.io',
            'PHP_AUTH_PW' => 'foobar',
        ));
        return $this->client;
    }

}