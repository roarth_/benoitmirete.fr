<?php

namespace Tests\AppBundle\Controller;

use Tests\ExtendedWebTestCase;

class DashboardControllerTest extends ExtendedWebTestCase
{
    /**
     * Test the app dashboard with an ANONYMOUS user
     */
    public function testIndexAsAnonymousUser()
    {
        $this->client = $this->initiateAnonymousClient();
        $this->client->followRedirects();
        $crawler = $this->client->request('GET', '/app/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Username")')->count());
    }

    /**
     * Test the app homepage with an ROLE_USER user
     */
    public function testIndexAsUser()
    {
        $this->client = $this->initiateUserClient();
        $this->client->followRedirects();
        $crawler = $this->client->request('GET', '/app/');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test the app dashboard with an ROLE_ADMIN user
     */
    public function testIndexAsAdminUser()
    {
        $this->client = $this->initiateAdminClient();
        $this->client->followRedirects();
        $crawler = $this->client->request('GET', '/app/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Dashboard")')->count());
    }
}