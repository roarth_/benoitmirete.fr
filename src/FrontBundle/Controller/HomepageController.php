<?php

namespace FrontBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomepageController extends Controller
{
    /**
     * The Frontend Entry Point
     *
     * @Route("/", name="front_homepage")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        return $this->render(':FrontBundle/homepage:index.html.twig');
    }
}
