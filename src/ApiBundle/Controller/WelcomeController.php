<?php

namespace ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class WelcomeController extends Controller
{
    /**
     * The Api's entry point
     *
     * @Route("/", name="api_welcome")
     * @Method({"GET"})
     */
    public function indexAction()
    {

        $counters = $this->get('app.statistics.service')->getSummaryStatistics();

        $data = [
            'code' => 200,
            'message' => 'welcome',
            'counters' => $counters
        ];

        return new JsonResponse($data);
    }
}
