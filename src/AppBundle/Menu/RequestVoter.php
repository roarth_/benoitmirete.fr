<?php

namespace AppBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class RequestVoter implements VoterInterface
{
    private $container;
    private $requestStack;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->requestStack = $requestStack;
    }

    public function matchItem(ItemInterface $item)
    {
        if ($item->getUri() === $this->requestStack->getCurrentRequest()->getRequestUri()) {
            return true;
        } else if($item->getUri() !== $this->container->get('router')->getContext()->getBaseUrl().'/'&& (substr($this->requestStack->getCurrentRequest()->getRequestUri(), 0, strlen($item->getUri())) === $item->getUri())) {
            return true;
        }
        return null;
    }
}