<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 25/08/2017
 * Time: 18:32
 */

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;

class MenuBuilder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('dashboard', ['label' => 'Dashboard', 'route' => 'app_dashboard', 'extras' => ['icon' => 'fa fa-television margin-right-5']]);
        $menu->addChild('albums', ['label' => 'Albums', 'route' => 'app_photo_albums', 'extras' => ['icon' => 'fa fa-folder-o margin-right-5']]);

        $menu->addChild('admin', ['label' => 'Administration', 'uri' => 'javascript:;', 'extras' => ['icon' => 'fa fa-lock margin-right-5']])->setAttributes(['class' => 'classic-menu-dropdown', 'aria-haspopup' => true])->setLinkAttributes(['data-hover' => 'megamenu-dropdown', 'data-close-others' => true])->setChildrenAttributes(['class' => 'dropdown-menu pull-left']);
        $menu['admin']->addChild('users', ['label' => 'Users', 'route' => 'app_administration_users', 'extras' => ['icon' => 'fa fa-users margin-right-10']]);

        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        return $menu;
    }
}