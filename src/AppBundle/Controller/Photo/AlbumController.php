<?php

namespace AppBundle\Controller\Photo;

use CoreBundle\Entity\Photo\AlbumEntity;
use CoreBundle\Form\Photo\AlbumEntityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AlbumController
 *
 * @Route("/albums")
 * @package AppBundle\Controller\Photo
 */
class AlbumController extends Controller
{
    /**
     * The Albums management page
     *
     * @Route("", name="app_photo_albums")
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction()
    {
        $albums = $this->getDoctrine()->getRepository('CoreBundle:Photo\AlbumEntity')->findAll();
        return $this->render(':AppBundle/photos/albums:index.html.twig', [
            'albums' => $albums
        ]);
    }

    /**
     * The Albums Details page
     *
     * @Route("/details/{album}", name="app_photo_albums_details")
     * @Method({"GET"})
     *
     * @param $album
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function detailsAction($album)
    {
        $album = $this->getDoctrine()->getRepository('CoreBundle:Photo\AlbumEntity')->findOneBySlug($album);
        return $this->render(':AppBundle/photos/albums:details.html.twig', [
            'album' => $album
        ]);
    }

    /**
     * The Albums Creation page
     *
     * @Route("/create", name="app_photo_albums_create")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function createAction(Request $request)
    {
        $album = new AlbumEntity();
        $albumForm = $this->createForm(AlbumEntityType::class, $album);

        $albumForm->handleRequest($request);
        if($albumForm->isSubmitted() && $albumForm->isValid()) {
            if(empty($album->getCover())) {
                $album->setCover('default_cover.jpg');
            } else {
                $file = $album->getCover();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('covers_directory'),
                    $fileName
                );
                $album->setCover($fileName);
            }


            $manager = $this->getDoctrine()->getManager();
            $manager->persist($album);
            $manager->flush();

            return $this->redirect($this->generateUrl('app_photo_albums'));
        }

        return $this->render(':AppBundle/photos/albums:create.html.twig', [
            'form' => $albumForm->createView()
        ]);
    }

    /**
     * The Albums Edition page
     *
     * @Route("/edit/{album}", name="app_photo_albums_edit")
     * @Method({"GET", "POST"})
     *
     * @param $album
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction($album, Request $request)
    {
        $album = $this->getDoctrine()->getRepository('CoreBundle:Photo\AlbumEntity')->findOneBySlug($album);
        $actualCover = $album->getCover();

        $album->setCover(new File($this->getParameter('covers_directory').'/'.$actualCover));
        $albumForm = $this->createForm(AlbumEntityType::class, $album);

        $albumForm->handleRequest($request);
        if($albumForm->isSubmitted() && $albumForm->isValid()) {

            if(empty($album->getCover())) {
                $album->setCover($actualCover);
            } else {
                $file = $album->getCover();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('covers_directory'),
                    $fileName
                );
                $album->setCover($fileName);

            }

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($album);
            $manager->flush();

            return $this->redirect($this->generateUrl('app_photo_albums'));
        }

        return $this->render(':AppBundle/photos/albums:edit.html.twig', [
            'album' => $album,
            'form' => $albumForm->createView()
        ]);
    }
}
