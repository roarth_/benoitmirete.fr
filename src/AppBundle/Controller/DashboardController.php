<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    /**
     * The application's Entry point
     *
     * @Route("/", name="app_dashboard")
     * @Method({"GET"})
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function indexAction()
    {
        $counters = $this->get('app.statistics.service')->getSummaryStatistics();
        return $this->render(':AppBundle/dashboard:index.html.twig', [
            'counters' => $counters
        ]);
    }
}
