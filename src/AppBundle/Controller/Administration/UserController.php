<?php

namespace AppBundle\Controller\Administration;

use CoreBundle\Entity\Administration\UserEntity;
use CoreBundle\Form\Administration\UserEntityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class UserController
 *
 * @Route("/users")
 * @package AppBundle\Controller\Administration
 */
class UserController extends Controller
{
    /**
     * The users management page
     *
     * @Route("", name="app_administration_users")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $users = $this->getDoctrine()->getRepository('CoreBundle:Administration\UserEntity')->findAll();
        return $this->render(':AppBundle/administration/users:index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * The users creation form page
     *
     * @Route("/create", name="app_administration_users_creation")
     * @Method({"GET", "POST"})
     */
    public function createAction()
    {
        $user = new UserEntity();
        $userForm = $this->createForm(UserEntityType::class, $user);

        return $this->render(':AppBundle/administration/users:create.html.twig', [
            'form' => $userForm->createView()
        ]);
    }

}