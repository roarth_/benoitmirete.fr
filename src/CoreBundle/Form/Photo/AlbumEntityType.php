<?php

namespace CoreBundle\Form\Photo;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use CoreBundle\Entity\Photo\AlbumEntity;

class AlbumEntityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'Album Name',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'Album Description',
                'attr' => [
                    'class' => 'form-control',
                    'rows' => 5
                ]
            ])
            ->add('isPublished', CheckboxType::class, [
                'required' => false,
                'label' => 'The Album is Published',
                'attr' => [
                    'class' => 'icheck icheckbox_minimal_purple'
                ]
            ])
            ->add('isPublic', CheckboxType::class, [
                'required' => false,
                'label' => 'The Album is Public',
                'attr' => [
                    'class' => 'icheck'
                ]
            ])
            ->add('cover', FileType::class, [
                'label' => 'Cover Picture',
                'required' => false,
                'data_class' => null
            ])
        ;
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AlbumEntity::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'corebundle_photo_albumentity';
    }


}
