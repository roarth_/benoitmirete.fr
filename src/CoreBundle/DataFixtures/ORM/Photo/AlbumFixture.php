<?php

namespace CoreBundle\DataFixtures\ORM\Photo;

use CoreBundle\Entity\Photo\AlbumEntity;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AlbumFixture extends AbstractFixture implements OrderedFixtureInterface
{
    protected $albums = [
        '0' => [
            'name' => 'Portraits',
            'description' => 'My Portraits',
            'published' => true,
            'public' => false
        ],
        '1' => [
            'name' => 'Landscapes',
            'description' => 'My Landscapes',
            'published' => false,
            'public' => true
        ],
    ];
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->albums as $key => $value) {

            $album = new AlbumEntity();
            $album->setName($value['name']);
            $album->setDescription($value['description']);
            $album->setIsPublished($value['published']);
            $album->setIsPublic($value['public']);

            $manager->persist($album);
            $manager->flush();
        }

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }

}