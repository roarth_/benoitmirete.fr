<?php

namespace CoreBundle\DataFixtures\ORM\Administration;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /** @var ContainerAwareInterface */
    private $container;

    protected $users = [
        '0' => [
            'username' => 'test-user',
            'email' => 'test-user@me.io',
            'password' => 'foobar',
            'role' => 'ROLE_USER',
            'name' => 'Test',
            'firstName' => 'User',
            'apiKey' => 'wx8jDl6pmEQmD26t'
        ],
        '1' => [
            'username' => 'test-admin',
            'email' => 'test-admin@me.io',
            'password' => 'foobar',
            'role' => 'ROLE_SUPER_ADMIN',
            'name' => 'Admin',
            'firstName' => 'User',
            'apiKey' => '9rhamhUZbSLacCS4'
        ],
    ];

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->users as $key => $value) {
            $userManager = $this->container->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setUsername($value['username']);
            $user->setEmail($value['email']);
            $user->setName($value['name']);
            $user->setFirstName($value['name']);
            $user->setApiKey($value['apiKey']);
            $user->setPlainPassword($value['password']);
            $user->addRole($value['role']);
            $user->setEnabled(true);
            $userManager->updateUser($user);

            print "Created: " . $value['username'] . " <" . $value['email'] . ">" . " as " . $value['role'] . "\n";
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }

}