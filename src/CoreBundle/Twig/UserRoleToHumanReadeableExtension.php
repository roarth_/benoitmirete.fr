<?php
namespace CoreBundle\Twig;

class UserRoleToHumanReadeableExtension extends \Twig_Extension
{
    private $rolesMapping = [
        'ROLE_USER' => [
            'value' => 'User',
            'color' => 'default'
        ],
        'ROLE_ADMIN' => [
            'value' => 'Co-Admin',
            'color' => 'info'
        ],
        'ROLE_SUPER_ADMIN' => [
            'value' => 'Super Admin',
            'color' => 'danger'
        ],
    ];

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('role', array($this, 'userRoleToHumanReadable')),
        );
    }

    public function userRoleToHumanReadable(array $roles)
    {
        $value = $this->rolesMapping[array_values($roles)[0]]['value'];
        $color = $this->rolesMapping[array_values($roles)[0]]['color'];

        return '<span class="label label-'. $color .'"> '. $value . ' </span>';
    }
}