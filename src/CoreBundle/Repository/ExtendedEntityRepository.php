<?php

namespace CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ExtendedEntityRepository extends EntityRepository
{
    /**
     * Count all the rows in the database
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function countAll()
    {
        return $this->createQueryBuilder('a')
            ->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();
    }

}