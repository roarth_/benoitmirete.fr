<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 28/08/17
 * Time: 17:24
 */

namespace CoreBundle\Service;

use Doctrine\ORM\EntityManager;

class StatisticsService
{

    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Returns the app summary statistics
     * Used on the API welcome
     * Used on the APP dashboard
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSummaryStatistics() {
        $data = [
            'albums' => $this->em->getRepository('CoreBundle:Photo\AlbumEntity')->countAll()
        ];

        return $data;
    }
}